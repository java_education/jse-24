package ru.t1.oskinea.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.oskinea.tm.api.repository.ICommandRepository;
import ru.t1.oskinea.tm.api.repository.IProjectRepository;
import ru.t1.oskinea.tm.api.repository.ITaskRepository;
import ru.t1.oskinea.tm.api.repository.IUserRepository;
import ru.t1.oskinea.tm.api.service.*;
import ru.t1.oskinea.tm.command.AbstractCommand;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.oskinea.tm.exception.system.CommandNotSupportedException;
import ru.t1.oskinea.tm.model.User;
import ru.t1.oskinea.tm.repository.CommandRepository;
import ru.t1.oskinea.tm.repository.ProjectRepository;
import ru.t1.oskinea.tm.repository.TaskRepository;
import ru.t1.oskinea.tm.repository.UserRepository;
import ru.t1.oskinea.tm.service.*;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.oskinea.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@comp.ru");
        @NotNull final User user = userService.create("user", "user", "user@comp.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(test.getId(), "project 1", "d1");
        projectService.create(test.getId(), "project 2", "d2");
        projectService.create(test.getId(), "project 3", "d3");
        projectService.create(test.getId(), "project 4", "d4");
        taskService.create(test.getId(), "task 1", "d1");
        taskService.create(test.getId(), "task 2", "d2");

        projectService.create(user.getId(), "user project 1", "ud1");
        taskService.create(user.getId(), "user task 1", "ut1");

        projectService.create(admin.getId(), "admin project 1", "ad1");
        taskService.create(admin.getId(), "admin task 1", "at1");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processArguments(@Nullable final String... arguments) {
        if (arguments == null || arguments.length < 1) return;
        @Nullable final String argument = arguments[0];
        if (argument == null || argument.isEmpty()) return;
        processArgument(argument);
        exit();
    }

    private void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("Enter command: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        initDemoData();
        initLogger();

        processArguments(args);
        processCommands();
    }

}
