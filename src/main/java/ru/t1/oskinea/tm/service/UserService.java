package ru.t1.oskinea.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.repository.IProjectRepository;
import ru.t1.oskinea.tm.api.repository.ITaskRepository;
import ru.t1.oskinea.tm.api.repository.IUserRepository;
import ru.t1.oskinea.tm.api.service.IUserService;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.exception.entity.UserNotFoundException;
import ru.t1.oskinea.tm.exception.field.*;
import ru.t1.oskinea.tm.exception.user.ExistsEmailException;
import ru.t1.oskinea.tm.exception.user.ExistsLoginException;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.model.Task;
import ru.t1.oskinea.tm.model.User;
import ru.t1.oskinea.tm.util.HashUtil;

import java.util.Collection;
import java.util.stream.Collectors;

public final class UserService extends AbstractService<User, IUserRepository>
        implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(final IUserRepository repository,
                       final IProjectRepository projectRepository,
                       final ITaskRepository taskRepository) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt((password)));
        user.setRole(Role.USUAL);
        return repository.add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @Nullable final Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Nullable
    @Override
    public User remove(@Nullable final User model) {
        if (model == null) return null;
        @Nullable final User user = super.remove(model);
        if (user == null) return null;
        @NotNull final String userId = user.getId();
        @NotNull final Collection<Task> tasks = taskRepository
                .findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
        taskRepository.removeAll(tasks);
        @NotNull final Collection<Project> projects = projectRepository
                .findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
        projectRepository.removeAll(projects);
        return user;
    }

    @Nullable
    @Override
    public User removeByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }

    @NotNull
    @Override
    public User updateUser(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
