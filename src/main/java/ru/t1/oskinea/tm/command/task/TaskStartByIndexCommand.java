package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Start task by index.";

    @NotNull
    private static final String NAME = "task-start-by-index";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
