package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.exception.field.StatusIncorrectException;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Change task status by id.";

    @NotNull
    private static final String NAME = "task-change-status-by-id";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS (");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("): ");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        if (status == null) throw new StatusIncorrectException();
        @NotNull final String userId = getUserId();
        getTaskService().changeTaskStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
