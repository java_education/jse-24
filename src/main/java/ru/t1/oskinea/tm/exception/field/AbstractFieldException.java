package ru.t1.oskinea.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException(@NotNull final String message) {
        super(message);
    }

    public AbstractFieldException(@NotNull final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            @NotNull final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
